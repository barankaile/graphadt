/****************************************************************************************
/**
*	List.c : Implementation of the List module for PA2
*	
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 4
*	Course: CMPS 101
*
*
*****************************************************************************************/

#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include "List.h"

// structs --------------------------------------------------------------------

// private NodeObj type
typedef struct NodeObj
{
	int data;
	struct NodeObj* next;
	struct NodeObj* prev;
}NodeObj;

// private Node type
typedef NodeObj* Node;

// private ListObj type
typedef struct ListObj
{
	Node front;
	Node back;
	Node cursor;
	int length;
	int cursorIndex;
}ListObj;	


// Constructors-Destructors ---------------------------------------------------

// newNode()
// Returns reference to new Node object. Initializes next, previous, and data fields.
// Private.
Node newNode(int node_data){
   Node N = malloc(sizeof(NodeObj));
   N->data = node_data;
   N->next = NULL;
   N->prev = NULL;
   return(N);
}

// freeNode()
// Frees heap memory pointed to by *pN, sets *pN to NULL.
// Private.
void freeNode(Node* pN){
   if( pN!=NULL && *pN!=NULL )
   {
      free(*pN);
      *pN = NULL;
   }
}

// newList()
// Returns reference to new empty List object.
List newList(void)
{
   List L;
   L = malloc(sizeof(ListObj));
   L->front = NULL;
   L->back = NULL;
   L->cursor = NULL;
   L->cursorIndex = -1;
   L->length = 0;
   return(L);
}

// freeList()
// Frees all heap memory associated with List *pL, and sets *pL to NULL.
void freeList(List* pL)
{
	if(pL!=NULL && *pL!=NULL)
	{
		while(length(*pL)!=0)
		{
			deleteFront(*pL);
		}
		free(*pL);
		*pL=NULL;
	}
}

// Access functions -----------------------------------------------------------

//length()
//returns the number of elements held in a List
int length(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling length() on NULL List reference\n");
		exit(1);
	}
	return(L->length);
}

//index()
//Returns the current index of the cursor
int index(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling index() on NULL List reference\n");
		exit(1);
	}
	return(L->cursorIndex);
}

//front()
//returns data held in the element at the front of the List
int front(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling front() on NULL List reference\n");
		exit(1);
	}
	L->cursor = L->front;
	if(L->cursor == NULL)
	{
		return -1;
	}
	return(L->cursor->data);
}

//back()
//returns data held in the element at the back of the List
int back(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling back() on NULL List reference\n");
		exit(1);
	}
	L->cursor = L->back;
	if(L->cursor==NULL)
	{
		return -1;
	}
	return(L->cursor->data);
}

//get()
//returns element currently highlighted by the cursor
int get(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling get() on NULL List reference\n");
		exit(1);
	}
	if(L->cursor==NULL)
	{
		printf("List Error: calling get() on NULL cursor element\n");
		exit(1);
	}
	else
	{
		return(L->cursor->data);
	}
}

//equals()
//checks if two lists are the same, (same number of elements, and same elements in each position
//regardless of cursor placement.
int equals(List A, List B)
{
	if(A==NULL || B==NULL)
	{
		printf("List Error: calling equals() on NULL List reference\n");
		exit(1);
	}
	if(length(A)!=length(B))
	{
		return 0;
	}
	else
	{
		int i;
		moveFront(A);
		moveFront(B);
		for(i=0; i<length(A);i++)
		{
			if(get(A)!=get(B))
			{
				return 0;
			}
			moveNext(A);
			moveNext(B);
		}
		return 1;
	}
		
}

// Manipulation procedures ----------------------------------------------------

//clear()
//Removes all elements from a List and frees the memory
//used by each element but does not free the memory used
//by the List itself. Will not make the passed in List null.
void clear(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling clear() on NULL List reference\n");
		exit(1);
	}
	while(L->length > 0)
	{
		deleteFront(L);
	}
}

//moveFront()
//Moves the cursor to the first elements in the List.
void moveFront(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling moveFront() on NULL List reference\n");
		exit(1);
	}
	if(L->length > 0)
	{
		L->cursor = L->front;
		L->cursorIndex = 0;
	}	
}

//moveBack()
//Moves the cursor to the final element in the List.
void moveBack(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling moveBack() on NULL List reference\n");
		exit(1);
	}
	if(L->length > 0)
	{
		L->cursor = L->back;
		L->cursorIndex = (L->length)-1;
	}
}

//movePrev()
//Moves the cursor one element back.
void movePrev(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling movePrev() on NULL List reference\n");
		exit(1);
	}
	if(L->cursor !=NULL)
	{
		if(L->cursor == L->front)
		{
			L->cursor = NULL;
			L->cursorIndex = -1;
		}
		else
		{
			L->cursor = L->cursor->prev;
			L->cursorIndex--;
		}
	}	
}

//moveNext()
//Moves the cursor one element forward.
void moveNext(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling moveNext() on NULL List reference\n");
		exit(1);
	}
	if(L->cursor != NULL)
	{
		if(L->cursor == L->back)
		{
			L->cursor = NULL;
			L->cursorIndex = -1;
		}
		else
		{			
			L->cursor = L->cursor->next;
			L->cursorIndex++;
		}
	}
}

//prepend()
//Adds a new element to the front of the List.
void prepend(List L, int data)
{
	Node N = newNode(data);
	if(L==NULL)
	{
		printf("List Error: calling prepend() on NULL List reference\n");
		exit(1);
	}
	if(L->length == 0)
	{
		L->front=L->back=N;
		L->length++;
	}
	else
	{
		N->next = L->front;
		L->front->prev = N;
		L->front = N;
		L->length++;
	}
}

//append()
//Adds a new element to the back of a List.
void append(List L, int data)
{
	Node N = newNode(data);
	if(L==NULL)
	{
		printf("List Error: calling append() on NULL List reference\n");
		exit(1);
	}
	if(L->length == 0)
	{
		L->front=L->back=N;
		L->length++;
	}
	else
	{
		N->prev = L->back;
		L->back->next = N;
		L->back = N;
		L->length++;
	}
}

//insertBefore()
//Adds a new element to the position just before the cursor.
void insertBefore(List L, int data)
{
	Node N = newNode(data);
	if(L==NULL)
	{
		printf("List Error: calling insertBefore() on NULL List reference\n");
		exit(1);
	}
	if(L->cursorIndex >= 0)
	{
		if(L->cursorIndex == 0)
		{
			prepend(L,data);
		}
		else
		{
			N->next = L->cursor;
			N->prev = L->cursor->prev;
			L->cursor->prev->next = N;
			L->cursor->prev = N;
			L->length++;
		}
	}
}

//insertAfter()
//Adds a new element to the position just after the cursor.
void insertAfter(List L, int data)
{
	Node N = newNode(data);
	if(L==NULL)
	{
		printf("List Error: calling insertAfter() on NULL List reference\n");
		exit(1);
	}
	if(L->cursorIndex >= 0)
	{
		if(L->cursorIndex == (L->length-1))
		{
			append(L,data);
		}
		else
		{
			N->prev = L->cursor;
			N->next = L->cursor->next;
			L->cursor->next->prev = N;
			L->cursor->next = N;
			L->length++;
		}
	}
}

//deleteFront()
//Deletes the element at the front of the List.
void deleteFront(List L)
{
	Node N = NULL;
	if(L==NULL)
	{
		printf("List Error: calling deleteFront() on NULL List reference\n");
		exit(1);
	}
	if(length(L) == 0)
	{
		printf("List Error: calling deleteFront() on List of size 0\n");
		exit(1);
	}
	else
	{
		N = L->front;
		if(length(L) > 1)
		{
			L->front = L->front->next;
			L->front->prev = NULL;
			L->length = length(L)-1;
		}
		else
		{
			L->front = L->back = NULL;
			L->length = length(L)-1;
		}
		freeNode(&N);
	}
}

//deleteBack()
//Deletes the element at the back of the List.
void deleteBack(List L)
{
	Node N = NULL;
	if(L==NULL)
	{
		printf("List Error: calling deleteBack() on NULL List reference\n");
		exit(1);
	}
	if(length(L) == 0)
	{
		printf("List Error: calling deleteBack() on List of size 0\n");
		exit(1);
	}
	else
	{
		N=L->back;
		if(length(L)>1)
		{
			L->back = L->back->prev;
			L->back->next = NULL;
			L->length--;
		}
		else
		{
			L->front = L->back = NULL;
			L->length--;
		}
		
		freeNode(&N);
	}
}

//delete()
//Deletes the element currently pointed to by the cursor.
void delete(List L)
{
	Node N = NULL;
	if(L==NULL)
	{
		printf("List Error: calling delete() on NULL List reference\n");
		exit(1);
	}
	if(length(L) == 0)
	{
		printf("List Error: calling delete() on List of size 0\n");
		exit(1);
	}		
	if(index(L) < 0)
	{
		printf("List Error: calling delete() on NULL cursor element 0\n");
		exit(1);
	}
	else
	{
		if(L->cursor == L->front)
		{
			deleteFront(L);
			L->cursor = NULL;
			L->cursorIndex = -1;
		}
		else if(L->cursor == L->back)
		{
			deleteBack(L);
			L->cursor = NULL;
			L->cursorIndex = -1;
		}
		else
		{
			N = L->cursor;
			L->cursor->prev->next = L->cursor->next;
			L->cursor->next->prev = N->prev;
			L->length--;
			L->cursor = NULL;
			L->cursorIndex = -1;
		}
		freeNode(&N);
	}
}

//printList()
//Prints the list to the specified output file as a series of space separated Strings.
void printList(FILE* out, List L)
{
	if(L==NULL)
	{
		printf("List Error: calling printList() on NULL List reference\n");
		exit(1);
	}
	if(out==NULL)
	{
		printf("List Error: calling printList() on NULL FILE* reference\n");
		exit(1);
	}
	moveFront(L);
	int i;
	for(i=0; i<length(L);i++)
	{		
			fprintf(out,"%d ", get(L));
			moveNext(L);
	}
}

//copyList()
//Returns a duplicate of the specified List.
List copyList(List L)
{
	if(L==NULL)
	{
		printf("List Error: calling copyList() on NULL List reference\n");
		exit(1);
	}
	List M = newList();
	int j;
	moveFront(L);
	for(j=0;j<length(L);j++)
	{
		append(M, get(L));
		moveNext(L);
	}
	return M;
}
	
	
	
		
	