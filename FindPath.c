/****************************************************************************************
/**
*	FindPath.c : Contains main for pa4. Functionality includes: reading from file, 
				building graph, and output of shortest paths (found via BFS) to specified output file
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 4
*	Course: CMPS 101
*
*
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Graph.h"

int main(int argc, char * argv[])
{
	FILE *in, *out;
	int vCount = 0; //size of graph
	int n1; //used for value storage
	int n2; //used for value storage
	
	if( argc != 3 ) //checking for correct number of inputs
	{
		printf("Invalid number of arguments");
		exit(1);
	}
	in = fopen(argv[1], "r");
	out = fopen(argv[2], "w");
	
	if(in==NULL)//checking for valid in file
	{
		printf("Unable to open file %s for reading\n", argv[1]);
		exit(1);
    }
	if(out==NULL) //checking for valid out file
	{
		printf("Unable to open file %s for writing\n", argv[2]);
		exit(1);
    }
	
	fscanf(in, "%d", &vCount); //reading first line from file to be used as size for Graph
	Graph G = newGraph(vCount);
	
	while(!feof(in))//building Graph based on each line containing 2 ints
	{
		fscanf(in , "%d %d" , &n1 , &n2);

		if((n1 == 0) && (n2 == 0))//checking for dummy line
		{
			break;
		}
		addEdge(G , n1 , n2);
	}
	printGraph(out , G);

	List L = newList(); //L will hold paths
	while(!feof(in))
	{
		fscanf(in , "%d %d" , &n1 , &n2); //reading source/destination from file
		if((n1 == 0) && (n2 == 0))//checking for dummy line
		{
			break;
		}
		BFS(G , n1);//BFS based on file specified beginning vertex
		getPath(L , G , n2); //path based on file specified terminal vertex
		if(getDist(G , n2) == INF)//checking if path is valid
		{
			fprintf(out , "Dist from %d to %d is infinity.\n" , n1 , n2);
			clear(L);
		}
		else//output of valid path
		{
			fprintf(out , "\nThe distance from %d to %d is %d" , n1 , n2 , getDist(G , n2));
			fprintf(out, "\nA shortest %d-%d path is: " , n1 , n2);
			printList(out , L);
			fprintf(out, "\n");
			clear(L);
		}
	}
	fclose(in); //closing files and freeing the List and Graph
	fclose(out);
	freeList(&L);
	freeGraph(&G);
}
//gcc -std=c99 List.h -std=c99 List.c Graph.c Graph.h FindPath.c