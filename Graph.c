/****************************************************************************************
/**
*	Graph.c : Implementation of the Graph module for PA4
*	
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 4
*	Course: CMPS 101
*
*
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "Graph.h"

#define WHITE 1
#define BLACK 2
#define GRAY 3
// Exported type --------------------------------------------------------------
typedef struct GraphObj
{
	List* adj;
	int* colors;
	int* parent;
	int* dist;
	
	int vCount;
	int eCount;
	int s;
} GraphObj;
// Constructors-Destructors ---------------------------------------------------
Graph newGraph(int n)
{
	Graph G = malloc(sizeof(struct GraphObj));
	G -> adj = calloc(n+1, sizeof(List));
	G -> colors = calloc(n+1, sizeof(int));
	G -> parent = calloc(n+1, sizeof(int));
	G -> dist = calloc(n+1, sizeof(int));	
	G -> vCount = n;
	G -> eCount = 0;
	G -> s = NIL;
	int i;
	for(i = 1; i < n+1; i++)
	{
		G -> adj[i] = newList();
		G -> colors[i] = WHITE;
		G -> parent[i] = NIL;
		G -> dist[i] = INF;
	}
	return G;
}

void freeGraph(Graph* pG)
{
	if(pG != NULL && *pG != NULL)
	{
		Graph temp = *pG;
		free(temp -> colors);
		free(temp -> parent);
		free(temp -> dist);
		int i;
		for(i = 1; i < getOrder(temp) + 1; i++)
		{
				freeList(&(temp -> adj[i]));		
		}
		free(*pG);
		*pG = NULL;
	}	
}

// Access functions -----------------------------------------------------------

int getOrder(Graph G)//number of vertices
{
	if(G == NULL)
	{
		printf("Graph Error: calling getOrder() on NULL Graph reference\n");
		exit(1);
	}
	return G -> vCount;
}

int getSize(Graph G)//number of edges
{
	if(G == NULL)
	{
		printf("Graph Error: calling getSize() on NULL Graph reference\n");
		exit(1);
	}
	return (G -> eCount);
}

int getSource(Graph G)//source vertex for BFS
{
	if(G == NULL)
	{
		printf("Graph Error: calling getSource() on NULL Graph reference\n");
		exit(1);
	}
	return(G -> s);
}

int getParent(Graph G, int u)//parent of specified vertex
{
	if(G == NULL)
	{
		printf("Graph Error: calling getParent() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || (u > getOrder(G)))//pre: 1<=U<=getOrder(G)
	{
		printf("Graph Error: calling getParent() on invalid vertex\n");
		exit(1);
	}
	return (G -> parent[u]);	
}

int getDist(Graph G, int u)//distance from source vertex to specified vertex
{
	if(G == NULL)
	{
		printf("Graph Error: calling getDist() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || (u > getOrder(G)))//pre: 1<=U<=getOrder(G)
	{
		printf("Graph Error: calling getDist() on invalid vertex\n");
		exit(1);
	}
	return (G -> dist[u]); //returns value of NIL if BFS has not yet been called
}

void getPath(List L, Graph G, int u)//path from source to specified vertex u
{
	if(L == NULL)
	{
		printf("Graph Error: calling getPath() on NULL List reference\n");
		exit(1);
	}
	if(G == NULL)
	{
		printf("Graph Error: calling getPath() on NULL Graph reference\n");
		exit(1);
	}
	if(getSource(G) == NIL)//pre: BFS() must be called prior to calling getPath()
	{
		printf("Graph Error: calling getPath() when getSource(G) == NIL\n");
		exit(1);
	}
	if(getSource(G) == u)
	{
		append(L, getSource(G));
	}
	else if(G -> parent[u] == NIL)
	{
		append(L, NIL);
	}
	else
	{
		getPath(L , G , (G -> parent[u]));
		append(L, u);
	}	
}

// Manipulation procedures ----------------------------------------------------

void makeNull(Graph G)//removes all edges from graph
{
	if(G == NULL)
	{
		printf("Graph Error: calling makeNull() on NULL Graph reference\n");
		exit(1);
	}
	int i;
	for(i = 0; i < getOrder(G) + 1; i++)
	{
		clear(G -> adj[i]);
	}
	G -> eCount = 0;
}

void orderedInsert(List L, int v)//helper method for adding edges in ascending order
{
	if(length(L) == 0)
	{
		append(L , v);
	}
	else
	{
		moveFront(L);
		int inserted = 0;
		while((index(L) != -1) && (inserted == 0))
		{
			if(get(L) > v)
			{
				insertBefore(L , v);
				inserted = 1;
			}
			moveNext(L);
		}
		if(inserted == 0)
		{
			append(L , v);
		}
	}		
}

void addEdge(Graph G, int u, int v)//adds an edge between the specified vertices u and v (undirected)
{
	if(G == NULL)
	{
		printf("Graph Error: calling addEdge() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || u > (getOrder(G)))//pre: 1<=u<=getOrder()
	{
		printf("Graph Error: calling addEdge() on invalid vertex u\n");
		exit(1);
	}
	if((v < 1) || (v > getOrder(G)))//pre: 1<=v<=getOrder()
	{
		printf("Graph Error: calling addEdge() on invalid vertex v\n");
		exit(1);
	}
	orderedInsert((G -> adj[u]) , v);
	orderedInsert((G -> adj[v]) , u);
	(G -> eCount)++;
}

void addArc(Graph G, int u, int v)//adds an edge between the specified vertices u and v (directed from u to v)
{
	if(G == NULL)
	{
		printf("Graph Error: calling addArc() on NULL Graph reference\n");
		exit(1);
	}
	if((u < 1) || u > (getOrder(G)))//pre: 1<=u<=getOrder()
	{
		printf("Graph Error: calling addArc() on invalid vertex u\n");
		exit(1);
	}
	if((v < 1) || (v > getOrder(G)))//pre: 1<=v<=getOrder()
	{
		printf("Graph Error: calling addArc() on invalid vertex v\n");
		exit(1);
	}
	orderedInsert((G -> adj[u]) , v);
	(G -> eCount)++;
}

void BFS(Graph G, int s)//performs BFS on G with respect to the starting vertex s
{
	if(G == NULL)
	{
		printf("Graph Error: calling BFS() on NULL Graph reference\n");
		exit(1);
	}
	
	List Q = newList();//FIFO queue
	List temp = newList();//holds adjacency lists of each vertex during loop
	int i;
	int num1;
	int num2;

	for(i = 0; i < getOrder(G) + 1; i++)
	{
		G -> colors[i] = WHITE;
		G -> parent[i] = NIL;
		G -> dist[i] = INF;
	}
	G -> s = s;
	G -> colors[s] = GRAY;
	G -> dist[s] = 0;
	G -> parent[s] = NIL;
	
	append(Q , s);

	while(length(Q) != 0)
	{
		num1 = front(Q);
		deleteFront(Q);
		
		temp = copyList(G -> adj[num1]);
		moveFront(temp);
		while(index(temp) != -1)
		{
			num2 = get(temp);
			moveNext(temp);
			if(G -> colors[num2] == WHITE)
			{
				G -> colors[num2] = GRAY;
				G -> dist[num2] = (G -> dist[num1]) + 1;
				G -> parent[num2] = num1;
				append(Q , num2);
			}
		}
		G -> colors[num1] = BLACK;
	}
	freeList(&temp);
}

void printGraph(FILE* out, Graph G)//sends G to the specified output file
{
	if(G == NULL)
	{
		printf("Graph Error: calling printGraph() on NULL Graph reference\n");
		exit(1);
	}
	if(out==NULL)
	{
		printf("List Error: calling printGraph() on NULL FILE* reference\n");
		exit(1);
	}
	int i;
	List temp;
	for(i = 1; i < getOrder(G) + 1; i++)
	{
		temp = copyList(G -> adj[i]);
		fprintf(out , "%d: " , i);
		printList(out , temp);
		fprintf(out , "\n");
	}
}





























	